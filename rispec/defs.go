package rispec

import (
	"fmt"
	"gitlab.com/mae.earth/renderman/fromscratch/library/format"
)

type Rter interface {
	String() string
}

/* RtBoolean */
type RtBoolean bool

/* String */
func (r RtBoolean) String() string {
	if !r {
		return "0"
	}
	return "1"
}

/* RtFloat */
type RtFloat float64

/* String */
func (r RtFloat) String() string {
	return format.Float64(float64(r))
}

/* RtInt */
type RtInt int

/* String */
func (r RtInt) String() string {
	return fmt.Sprintf("%d", r)
}

/* RtColor */
type RtColor [3]float64

/* String */
func (r RtColor) String() string {
	return fmt.Sprintf("[%s]", format.Float64([3]float64(r)))
}

/* RtPoint */
type RtPoint [3]float64

/* String */
func (r RtPoint) String() string {
	return fmt.Sprintf("[%s]", format.Float64([3]float64(r)))
}

/* RtVector */
type RtVector [3]float64

/* String */
func (r RtVector) String() string {
	return fmt.Sprintf("[%s]", format.Float64([3]float64(r)))
}

/* RtNormal */
type RtNormal [3]float64

/* String */
func (r RtNormal) String() string {
	return fmt.Sprintf("[%s]", format.Float64([3]float64(r)))
}

/* RtHpoint */
type RtHpoint [4]float64

/* String */
func (r RtHpoint) String() string {
	return fmt.Sprintf("[%s]", format.Float64([4]float64(r)))
}

/* RtMatrix */
type RtMatrix [16]float64

/* String */
func (r RtMatrix) String() string {
	return fmt.Sprintf("[%s]", format.Float64([16]float64(r)))
}

/* RtBasis */
type RtBasis [16]float64

/* String */
func (r RtBasis) String() string {
	return fmt.Sprintf("[%s]", format.Float64([16]float64(r)))
}

/* RtBound */
type RtBound [6]float64

/* String */
func (r RtBound) String() string {
	return fmt.Sprintf("[%s]", format.Float64([6]float64(r)))
}

/* RtInterval */
type RtInterval [2]float64

/* String */
func (r RtInterval) String() string {
	return fmt.Sprintf("[%s]", format.Float64([2]float64(r)))
}

/* RtToken */
type RtToken string

/* String */
func (r RtToken) String() string {
	return fmt.Sprintf("%q", string(r))
}

/* RtString */
type RtString string

/* String */
func (r RtString) String() string {
	return fmt.Sprintf("%q", string(r))
}

/* Boolean */
func Boolean(yesno bool) RtBoolean {
	return RtBoolean(yesno)
}

/* Float */
func Float(n float64) RtFloat {
	return RtFloat(n)
}

/* Int */
func Int(d int) RtInt {
	return RtInt(d)
}

/* Color */
func Color(r ...float64) RtColor {
	if len(r) == 0 {
		return RtColor{0.0, 0.0, 0.0}
	}

	if len(r) == 1 {
		return RtColor{r[0], r[0], r[0]}
	}

	if len(r) == 2 {
		return RtColor{r[0], r[1], r[1]}
	}

	return RtColor{r[0], r[1], r[2]}
}

/* Point */
func Point(x, y, z float64) RtPoint {
	return RtPoint{x, y, z}
}

/* Vector */
func Vector(vx, vy, vz float64) RtVector {
	return RtVector{vx, vy, vz}
}

/* Normal */
func Normal(nx, ny, nz float64) RtNormal {
	return RtNormal{nx, ny, nz}
}

/* Hpoint */
func Hpoint(x, y, z, w float64) RtHpoint {
	return RtHpoint{x, y, z, w}
}

/* Matrix */
func Matrix(r ...float64) RtMatrix {

	out := [16]float64{}

	min := len(r)
	if min > 16 {
		min = 16
	}

	for i := 0; i < min; i++ {
		out[i] = r[i]
	}
	return RtMatrix(out)
}

/* Basis */
func Basis(r ...float64) RtBasis {

	out := [16]float64{}

	min := len(r)
	if min > 16 {
		min = 16
	}

	for i := 0; i < min; i++ {
		out[i] = r[i]
	}
	return RtBasis(out)
}

/* Bound */
func Bound(r ...float64) RtBound {

	out := [6]float64{}

	min := len(r)
	if min > 6 {
		min = 6
	}
	for i := 0; i < min; i++ {
		out[i] = r[i]
	}
	return RtBound(out)
}

/* Interval */
func Interval(s, t float64) RtInterval {
	return RtInterval{s, t}
}

/* Token */
func Token(str string) RtToken {
	return RtToken(str)
}

/* String */
func String(str string) RtString {
	return RtString(str)
}
