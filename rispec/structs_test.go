package rispec

import (

	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_RiSpecTools(t *testing.T) {
	Convey("FindRiCommand", t, func() {
		Convey("Declare",func() {

			info,err := FindRiCommand("Declare")
			So(err,ShouldBeNil)
			So(info,ShouldNotBeNil)
			So(len(info.Values),ShouldEqual,2)
			So(info.HasParameterlist,ShouldBeFalse)

		})
	})
}
