package rispec

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_RiSpec(t *testing.T) {
	Convey("RiSpec", t, func() {
		Convey("RtBoolean", func() {
			So(Boolean(true), ShouldEqual, RtBoolean(true))
			So(Boolean(false), ShouldEqual, RtBoolean(false))
			Convey("string()", func() {
				So(Boolean(true).String(), ShouldEqual, "1")
				So(Boolean(false).String(), ShouldEqual, "0")

				fmt.Printf("\n\tboolean(true) = %q\n", Data(Boolean(true)))
			})
		})

		Convey("RtFloat", func() {
			So(Float(0.0), ShouldEqual, RtFloat(0.0))
			So(Float(1.0), ShouldEqual, RtFloat(1.0))
			So(Float(-1.0), ShouldEqual, RtFloat(-1.0))
			Convey("string()", func() {
				So(Float(0.0).String(), ShouldEqual, "0")
				So(Float(1.0).String(), ShouldEqual, "1")
				So(Float(-1.0).String(), ShouldEqual, "-1")

				fmt.Printf("\n\tfloat(-0.09876) = %q\n", Data(Float(-0.09876)))
			})
		})

		Convey("RtColor", func() {
			Convey("(-)", func() {
				So(Color(), ShouldEqual, RtColor{0.0, 0.0, 0.0})
			})
			Convey("(r)", func() {
				So(Color(1.0), ShouldEqual, RtColor{1.0, 1.0, 1.0})
			})
			Convey("(r,g)", func() {
				So(Color(1.0, 0.5), ShouldEqual, RtColor{1.0, 0.5, 0.5})
			})
			Convey("(r,g,b)", func() {
				So(Color(0.0, 0.0, 0.0), ShouldEqual, RtColor{0.0, 0.0, 0.0})
				So(Color(1.0, 1.0, 1.0), ShouldEqual, RtColor{1.0, 1.0, 1.0})
				So(Color(-1.0, -1.0, -1.0), ShouldEqual, RtColor{-1.0, -1.0, -1.0})
			})

			Convey("string()", func() {
				So(Color(0.0, 0.0, 0.0).String(), ShouldEqual, "[0 0 0]")
				So(Color(1.0, 1.0, 1.0).String(), ShouldEqual, "[1 1 1]")
				So(Color(-1.0, -1.0, -1.0).String(), ShouldEqual, "[-1 -1 -1]")

				fmt.Printf("\n\tcolor(0.1 0.25 0.345) = %q\n", Data(Color(0.1, 0.25, 0.345)))
			})
		})

		Convey("RtMatrix", func() {
			Convey("(-)", func() {
				So(Matrix(), ShouldEqual, RtMatrix{0.0})
			})
			Convey("(r)", func() {
				So(Matrix(1.0), ShouldEqual, RtMatrix{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0})
			})
			Convey("string()", func() {
				So(Matrix().String(), ShouldEqual, "[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]")
				So(Matrix(1.0, 1.0, 1.0).String(), ShouldEqual, "[1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0]")

				fmt.Printf("\n\tmatrix(0.1 0.25 0.345) = %q\n", Data(Matrix(0.1, 0.25, 0.345)))
			})
		})

	})
}
