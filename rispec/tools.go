package rispec

import (
	"fmt"
	"strconv"
)

/* Data */
func Data(r Rter) string {
	if r == nil {
		return "[--]"
	}

	out := r.String()
	if out == "" {
		return "[--]"
	}

	if out[0] == '[' && out[len(out)-1] == ']' {
		return out
	}

	return "[" + out + "]"
}


/* ParseTokenSpecification */
func ParseTokenSpecification(token RtToken) (RtToken,RtToken,RtString) {
	class,typeof,name,_ := ParseValidateTokenSpecification(token)
	
	return class,typeof,name
}

/* ParseValidateTokenSpecification */
func ParseValidateTokenSpecification(token RtToken) (class RtToken,typeof RtToken,name RtString,err error) {
/* Token spec := [class] type name
	   * where class is either "constant" | "uniform" | "varying" | "vertex"
	   * where type is a golang (C) underlying type or a RenderMan type
		 * see pg. 13
	*/

	
	class = RtToken("uniform")
	typeof = RtToken("string")
	name = RtString("")
	err = nil

	if len(token) == 0 {
		return 
	}

	str := string(token)
	parts := 0
	mode := "name"

	for i := 0; i < len(str); i++ {
		if str[i] == ' ' {
			parts ++
			switch mode {
				case "name":
					typeof = RtToken(string(name))
					name = RtString("")
					mode = "type"	
				break
				case "type":
					class = typeof
					typeof = RtToken(string(name))
					name = RtString("")
					mode = "class"
				break
			}
			continue
		}
		name = RtString(string(name) + string(str[i]))
	}
	parts++

	if parts > 3 {
		err = fmt.Errorf("Invalid Token")
		return
	}

	if parts >= 2 {
		if typeof == "-" {
			err = fmt.Errorf("Invalid Type")
			return
		}
	
		array := ""
		for i := 0; i < len(typeof); i++ {
			if typeof[i] == '[' {
				array = string(typeof)[i:]
			}
		}

		if len(array) > 0 {
			if array[0] == '[' && array[len(array) - 1] == ']' {
				ni,err2 := strconv.Atoi(array[1 : len(array) - 1])
				if err2 != nil {
					err = err2
					return
				}
			
				switch ni {
					case 1,3,4,6,16:
						/* do nothing */
					break
					default:
						err = fmt.Errorf("Invalid Typed array, %s", string(typeof))
					break
				}

			} else {
				err = fmt.Errorf("Invalid Typed array")
			}	

			if err != nil {
				return
			}
		}
	}

	switch string(typeof) {
		case "string","point","normal","hpoint","interval","int","integer","float","boolean","bool",
					"color","vector","matrix","basis","lighthandle","archivehandle","objecthandle":
		break
		default:
			err = fmt.Errorf("Invalid token Type -- %q",string(typeof))
		break
	}

	if parts == 3 {
		switch class {
			case "constant","uniform","varying","vertex","reference":
		break
		default:
			err = fmt.Errorf("Invalid token class -- %q",string(typeof))
		break
		}
	}
	return
}
 

	

