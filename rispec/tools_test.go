package rispec

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Tools(t *testing.T) {
	Convey("Tools",t,func() {
		Convey("ParseTokenSpecification",func() {
			Convey("foo",func() {
				class,typeof,name := ParseTokenSpecification(RtToken("foo"))
				So(class,ShouldEqual,RtToken("uniform"))
				So(typeof,ShouldEqual,RtToken("string"))
				So(name,ShouldEqual,RtString("foo"))
			})
			Convey("float bar",func() {
				class,typeof,name := ParseTokenSpecification(RtToken("float bar"))
				So(class,ShouldEqual,RtToken("uniform"))
				So(typeof,ShouldEqual,RtToken("float"))
				So(name,ShouldEqual,RtString("bar"))
			})
			Convey("varying float bar",func() {
				class,typeof,name := ParseTokenSpecification(RtToken("varying float bar"))
				So(class,ShouldEqual,RtToken("varying"))
				So(typeof,ShouldEqual,RtToken("float"))
				So(name,ShouldEqual,RtString("bar"))
			})
			Convey("float[3] foo",func() {
				class,typeof,name := ParseTokenSpecification(RtToken("float[3] foo"))
				So(class,ShouldEqual,RtToken("uniform"))
				So(typeof,ShouldEqual,RtToken("float[3]"))
				So(name,ShouldEqual,RtString("foo"))
			})
		})
		Convey("ParseValidateTokenSpecification",func() {
			Convey("bad specification",func() {
				_,_,_,err := ParseValidateTokenSpecification(RtToken("float[5] foo"))
				So(err.Error(),ShouldEqual,"Invalid Typed array, float[5]")
			})
		})

	})
}



