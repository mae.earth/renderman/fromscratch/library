package rib

/* MAJOR TODO: add line numbers, char positions etc */

/* Param */
type Param struct {
	Name string				`rispec:"name"`
	Value interface{}	`rispec:"value"`
}


/* General */
type General struct {
	RIB string `rispec:"rib"` /* denotes a RIB command */
	Values []Param	`rispec:"values"`
	Params []Param	`rispect:"params"`
}

type RecordType string

const (
	Structure = RecordType("structure")
	Comment = RecordType("comment")
	Verbatim = RecordType("verbatim")
)


/* Record */
type Record struct {
	Type RecordType
	Format string
	Args []interface{}
}

/* FrameBlock */
type FrameBlock struct {
	Frame int `rispec:"value"`
	Children []interface{}	`rispec:"children"`
}

/* WorldBlock */
type WorldBlock struct {
	Children []interface{}	`rispec:"children"`
}

/* AttributeBlock */
type AttributeBlock struct {
	Children []interface{}	`rispec:"children"`
}

/* ArchiveBlock */
type ArchiveBlock struct {
	Name string `rispec:"value"`
	Children []interface{}	`rispec:"children"`
}


