package rib

import (
	"strings"
	"fmt"
	"strconv"
//	"reflect"

	"gitlab.com/mae.earth/renderman/fromscratch/library/rispec"	
)


const RIBParserVersion float64 = 3.04

/* tokeniser */
const (
	spaceToken string = "<space>"
	wordToken string = "<word>"
	literalToken string = "<literal>"
	bracketToken string = "<bracket>"
)

const (
	numberLexical string = "[number]"
	stringLexical string = "[string]"
	alphaLexical string = "[alpha]"
	openLexical string = "[open]"
	closeLexical string = "[close]"
	delimiterLexical string = "[--]"
)

const (
	wsParser string = "--"
	versionParser string = "version"
	beginCommandParser string = "--begin--"
	endCommandParser string = "--end--"
	structureParser string = "##"
	commentParser string = "#"
	parameterlistParser string = "--params--"
)

type token struct {
	Type string       /* tokensier type spaceToken | wordToken */
	Lexical string		/* what the lexer thinks */
	Parser string     /* whate the parser thinks */
	RI string					/* what the RI Spec says it should be, string,float etc  */
	RIName string    

	Line int
	Begin int 
	End int
}

func (t token) String() string {
	return fmt.Sprintf("L%05d:P%05d-%05d\t%10s",t.Line,t.Begin,t.End,t.Type)
}

func (t token) Print(in string) string {
	return fmt.Sprintf("L%05d:P%05d-%05d\t%10s\t%s",t.Line,t.Begin,t.End,t.Type,in[t.Begin:t.End])
}

/* microcontext, required mainly for RiDeclare */
type microcontext struct {
	declarations map[string]string /* token name = full specced token */
}

func (ctx *microcontext) Declare(name,spec string) (string,error) {

	_,_,_,err := rispec.ParseValidateTokenSpecification(rispec.RtToken(fmt.Sprintf("%s %s",spec,name)))
	if err != nil {
		return fmt.Sprintf("%s %s",spec,name),err
	}

	ctx.declarations[ name ] = spec
	return fmt.Sprintf("%s %s",spec,name),nil
}

func (ctx *microcontext) Lookup(name string) (string,bool) {

	if value,found := ctx.declarations[ name ]; found {
		return fmt.Sprintf("%s %s",value,name),true
	}

	return name,false
}

func (ctx *microcontext) Check(token string) (string,error) {

	/* first we do a parse and validate check */
	class,typeof,name,err := rispec.ParseValidateTokenSpecification(rispec.Token(token))
	if err != nil {
		return token,err
	}

	/* then we check if the token has been already declared */
	if ntoken,found := ctx.Lookup(string(name)); found {
		return ntoken,nil
	}

	return fmt.Sprintf("%s %s %s",string(class),string(typeof),string(name)),nil
}



/* ParseString */
func ParseString(toparse string) ([]interface{},error) {
	/* we use a hand-crafted parser to avoid all the setup cost of using a generator
   * to machine produce the parser */
	
	universe := make([]interface{},0)

	ctx := &microcontext{}
	ctx.declarations = make(map[string]string,0) 

	strip := func(b,e int) string {
		str := toparse[b:e]
		if str[0] == '"' && str[len(str) - 1] == '"' {
			return str[1:len(str) - 1]	
		}
		return str
	}

	snip := func(str string) string {
		if len(str) < 7 {
			return str
		}
		return str[:7] + "..."
	}

	word := ""	
	literal := false
	line := 1

	/* tokeniser */
	tokens := make([]token,0)

	for i,c := range toparse {
		ch := string(c)

		switch ch {
			case "\"":
				literal = !literal
				word += string(ch)
			break
			case " ","\t","\n","[","]":
				if !literal {
					if len(word) > 0 {
										
						t := wordToken
						if word[0] == '"' && word[len(word) - 1] == '"' {
							t = literalToken
						}

						tokens = append(tokens,token{Type:t, Line:line, Begin: i - len(word),End:i})
						word = ""					
					}
						
					t := spaceToken

					if ch == "[" || ch == "]" {
						t = bracketToken
					}
					tokens = append(tokens,token{Type:t,Line:line,Begin:i,End:i})
					

				} else {
					word += ch
				}

				if ch == "\n" {

					line ++
				}
			break
			default:
				word += string(ch)
			break
		}
	}
	/* tail */
	if len(word) > 0 {
		tokens = append(tokens,token{Type:wordToken,Line:line,Begin: len(toparse) - len(word),End:len(toparse)})
	}

	/* tokens */
	for _,t := range tokens {
		tag := toparse[t.Begin:t.End]
		if t.Begin == t.End {
			tag = string(toparse[t.Begin])
		}
		fmt.Printf("L%05d:P%05d-%05d\t%10s\t%q\n",t.Line,t.Begin,t.End,t.Type,tag)
	}

	/* lexer */
	ntokens := make([]token,0)
	
	isNumber := func(b,e int) bool {
		/* TODO : scan through all */
		if e - b <= 0 {
			return false
		}
		o := false
		switch toparse[b] {
			case '-','+','.','0','1','2','3','4','5','6','7','8','9' :
				o = true
			break
		}
		return o
	}


	for _,t := range tokens {
		
		lex := "-"

		switch t.Type {
			case spaceToken:
				lex = delimiterLexical
			break
			case wordToken:
				if isNumber(t.Begin,t.End) {
					lex = numberLexical
				} else {
					lex = alphaLexical
				}
			break
			case literalToken:
				lex = stringLexical
			break
			case bracketToken:
				if toparse[t.Begin] == '[' {
					lex = openLexical
				} else {
					lex = closeLexical
				}
			break
		}

		/* compact space tokens */
		ap := true
		if len(ntokens) > 0 && t.Type == spaceToken {
			if ntokens[len(ntokens) - 1].Type == spaceToken {
	
				line := ntokens[len(ntokens) - 1].Line
				begin := ntokens[len(ntokens) - 1].Begin
				
				ntokens[len(ntokens) - 1] = token{Type: spaceToken, Lexical: delimiterLexical,Line: line,Begin: begin,End:t.End}	

				ap = false
			}
		}
		if ap {
			ntokens = append(ntokens,token{Type: t.Type, Lexical: lex, Begin: t.Begin, End: t.End, Line: t.Line}) 
		}
		
	}
	
	for _,t := range ntokens {
		tag := toparse[t.Begin:t.End]
		if t.Begin == t.End {
			tag = string(toparse[t.Begin])
		}
		fmt.Printf("L%05d:P%05d-%05d\t%10s : %10s\t%q\n",t.Line,t.Begin,t.End,t.Type,t.Lexical,tag)
	}

	/* 1st pass parser, main job is to find RI Commands, values and parameterlist */
	commands := strings.Split(rispec.Commands,",")
	started := false /* have we seen a command yet */
	tokens = make([]token,0)

	currentCommand := "-"
	previousCommand := "-"

	openBracket := false
	var info *rispec.RiStructInformation
	
	stack := make([]token,0)
	current := 0

	for _,t := range ntokens {

		p := wsParser
		ri := ""
		riname := ""
	
		/* go through and look more closely at any alphaLexicals we found */
		switch t.Lexical {
		case alphaLexical:
		
			/* check for version; this is a special case for the parser */
			if toparse[t.Begin:t.End] == "version" {
				p = versionParser
				previousCommand = currentCommand
				currentCommand = "version"
				stack = make([]token,0)
			}

			if p == wsParser {
				for _,command := range commands {
				
					if toparse[t.Begin:t.End] == command {
						p = beginCommandParser
				
						/* allow special case for Declare; we need to call this command 
						 * as early as possible so that read-in tokens can be properly
             * checked. 
						 */

						/* lookup command */
						i,err := rispec.FindRiCommand(command) 
						if err != nil {
							return nil,fmt.Errorf("parser error, unknown command %q -- %v",command,err)
						}

						info = i
						ri = info.Name
						current = 0

						previousCommand = currentCommand
						currentCommand = command						

						break
					}		
				}
			}

			if p == wsParser {
				/* check for comment */
				str := toparse[t.Begin:t.End] 
				if len(str) == 1 {
					if str == "#" {
						p = commentParser
					}
				} else if len(str) > 2 {
					if str[0] == '#' {
						p = commentParser
						if str[1] == '#' {
							p = structureParser
						}
					}
				}	else { /* if, edge case of 2 */
					if str == "##" {
						p = structureParser
					}
				}			
			}

		break	
		case stringLexical:
			/* use the command prototype to work out what this type should be, either an actual string or a token.
			 * At this point if it is a token we should go ahead and parse it for RtToken specification. */

			/* let's attempt to see if this is token */
			full,err := ctx.Check(strip(t.Begin,t.End))
			if err != nil {
				return nil,fmt.Errorf("token parse error %q -- %v",strip(t.Begin,t.End),err)
			}

			ri = full

			if info != nil {
				if len(info.Values) > current { 
		
						riname = info.Values[current].Name 
				} else {

					riname = fmt.Sprintf("%d < %d",len(info.Values),current)
				}
			} else {
				riname = "info == nil"
			}

		
			
			current ++
		break
		case numberLexical:
			/* the trouble with RIB numbers is that the notation is a little too reductive, so we have to look at
			 * the command in question and use that to denote what type it is supposed to be. 
			 */

			/* TODO: check with current command to see if this is expected */

			current ++
		break
		case openLexical:
			/* set the open bracket bool */
			if openBracket {
				return nil,fmt.Errorf("further bracket found")
			}
			openBracket = true
		break
		case closeLexical:
			/* generate an error if open bracket bool is not true */
			if !openBracket {
				return nil,fmt.Errorf("bracket was not opened first")
			}
			openBracket = false
			current ++
		break
		}	

		if p == beginCommandParser { /* inject an end command token for previous command */

			/* check if openBracket first */
			if openBracket {
				return nil,fmt.Errorf("opened bracket")
			}

			if previousCommand == "version" {

					if len(stack) == 0 {
						return nil,fmt.Errorf("version error -- expecting 1 argument but got %d",len(stack))
					}

					if stack[0].Lexical != numberLexical {
						return nil,fmt.Errorf("version error -- expecting a number but was %q",stack[0].Lexical)			
					}

					ver,err := strconv.ParseFloat(toparse[stack[0].Begin:stack[0].End],64)
					if err != nil {
						return nil,fmt.Errorf("version error -- %v",err)
					}

					if ver > RIBParserVersion {
						return nil,fmt.Errorf("version error -- parser can not proceed with this version (%f > %f)",ver,RIBParserVersion)
					}
				}

			if started {

				/* special case for any Declare calls. */
				if previousCommand == "Declare" {

					if len(stack) < 2 {

						return nil,fmt.Errorf("Declare error -- expecting 2 arguments but got %d",len(stack))
					}

					if stack[0].Lexical != stringLexical && stack[1].Lexical != stringLexical {
	
						return nil,fmt.Errorf("Declare error - expecting string tokens but got %q and %q",stack[0].Lexical,stack[1].Lexical)
					}

					if _,err := ctx.Declare(strip(stack[0].Begin,stack[0].End), strip(stack[1].Begin,stack[1].End)); err != nil {

						return nil,fmt.Errorf("Declare error -- %v",err)
					}					
				}


				/* TODO: add command information here */ 

				tokens = append(tokens,token{Type:wordToken,Lexical:alphaLexical,Parser:endCommandParser,Line:-1})
			}
			/* empty previous commands argument stack */ 
			stack = make([]token,0)
			started = true
		}

		/* start dropping the space tokens */
		if t.Type != spaceToken {

			tokens = append(tokens,token{Type:t.Type,Lexical:t.Lexical,Parser:p,Line:t.Line,Begin:t.Begin,End:t.End,RI:fmt.Sprintf("%d:%s",current,ri),RIName:riname})

			if p != beginCommandParser && p != versionParser {

				stack = append(stack,token{Type:t.Type,Lexical:t.Lexical,Parser:p,Line:t.Line,Begin:t.Begin,End:t.End,RI:ri,RIName:riname})
			}
		}
	}

	for _,t := range tokens {
		tag := toparse[t.Begin:t.End]
		if t.Line == -1 {
			tag = "---"
		} else {		
			if t.Begin == t.End {
				tag = string(toparse[t.Begin])
			}
		}
		fmt.Printf("L%05d:P%05d-%05d %10s:%10s=%10s (%10s) [%10s] | %q\n",t.Line,t.Begin,t.End,t.Type,t.Lexical,t.Parser,snip(t.RI),t.RIName,tag)
	}

	


	return universe,nil
}






