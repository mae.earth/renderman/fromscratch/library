package rib

/* TODO: 
 * use ribcat and prman to get the proper error codes for malformed rib
 * compare speed of the parsers 
 */

import (
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_RIBParser(t *testing.T) {
	Convey("RIBParser",t,func() {
		universe,err := ParseString(rib_example_001)
		So(err,ShouldBeNil)
		So(universe,ShouldNotBeNil)

		So(len(universe),ShouldEqual,3) /* ##RenderMan RIB (1), version 3.04 (2) & FrameBegin 1 + (3) */

	})
}

const rib_example_001 string = `
##RenderMan RIB
version 3.04
Declare "diffuseColor" "color"
FrameBegin 1
	Format 640 480 1
	Display "test.exr" "rgba" 
	WorldBegin
		AttributeBegin
			Attribute "identifier" "string name" ["key_light"] # this is our key light
			Light "PxrSphereLight" "light" "float intensity" [8]
		AttributeEnd
		AttributeBegin
			Attribute "identifier" "string name" ["hero"]
			# this is a "sphere"
			Bxdf "PxrDiffuse" "name" "diffuseColor" [1 0 0]
			Sphere 1 1 -1 360 "float foo" [1.23] "string bar" ["foo"]
		AttributeEnd
	WorldEnd
FrameEnd
`
