package plugins

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"fmt"

	"gitlab.com/mae.earth/renderman/fromscratch/library/rispec"
)

func rib(list ...rispec.Rter) string {

	out := ""
	for i,param := range list {
		if len(out) > 0 {
			out += " "
		}
		if i % 2 != 0 {
			out += rispec.Data(param)
		} else {
			out += param.String()
		}
	}

	return out
}



func Test_ArgsParser(t *testing.T) {

	Convey("Inline Args Parse", t, func() {

		plugin, err := parse("PxrCamera", []byte(argsExample1))
		So(err, ShouldBeNil)
		So(plugin, ShouldNotBeNil)

		So(plugin.Name, ShouldEqual, "PxrCamera")
		So(plugin.ShaderType, ShouldEqual, "projection")
		So(plugin.NodeId, ShouldEqual, "")
		So(plugin.Classification, ShouldEqual, "")

		So(len(plugin.Pages), ShouldEqual, 6)
		So(len(plugin.Params), ShouldEqual, 0)
		So(len(plugin.Outputs), ShouldEqual, 0)

		So(string(plugin.Help), ShouldEqual, `A camera model that approximates a number of real world physical
    effects.  This supports all of the traditional prman perspective camera
    settings including shaped motion blur and bokeh.`)

	})

	Convey("Load", t, func() {

		plugin, err := Load("PxrCamera")
		So(err, ShouldBeNil)
		So(plugin, ShouldNotBeNil)

		So(plugin.Name, ShouldEqual, "PxrCamera")
		So(plugin.ShaderType, ShouldEqual, "projection")
		So(plugin.NodeId, ShouldEqual, "1053314")
		So(plugin.Classification, ShouldEqual, "RenderMan/projection")

		So(len(plugin.Pages), ShouldEqual, 6)
		So(len(plugin.Params), ShouldEqual, 0)
		So(len(plugin.Outputs), ShouldEqual, 0)

		So(plugin.Parameterlist(),ShouldBeEmpty)
		

		Convey("Set", func() {

			nplugin := plugin.Set("fov", 12.34)
			So(nplugin, ShouldNotBeNil)

			param, ok := nplugin.Lookup("fov")
			So(ok, ShouldBeTrue)
			So(param.Current.(float64), ShouldEqual, 12.34)

			parameterlist := nplugin.Parameterlist()
			So(parameterlist,ShouldNotBeNil)
			So(len(parameterlist),ShouldEqual,2)

			So(rib(parameterlist...),ShouldEqual,`"float fov" [12.34]`)
		})
	})

	Convey("Problem Loads",t,func() {
		/* first arraySize="16" */
		Convey("PxrBlockerLightFilter.args",func() {
			plugin,err := Load("PxrBlockerLightFilter")
			So(err,ShouldBeNil)
			So(plugin,ShouldNotBeNil)
		})
		/* first mult=[LightFilter] */
		Convey("PxrCombinerLightFilter.args",func() {
			plugin,err := Load("PxrCombinerLightFilter")
			So(err,ShouldBeNil)
			So(plugin,ShouldNotBeNil)
		})
	})




	Convey("Scan",t,func() {
	
		info,err := Scan("")
		So(err,ShouldBeNil)
		So(info,ShouldNotBeNil)

		fmt.Printf("\n\n%s\n",info.PrettyPrint())
	})

	Convey("clean",t,func() {

		/* before the benchmarks clean the cache */
		Configure(Configuration{Cache: true})
	})


}


func Benchmark_InlinePxrCamera(b *testing.B) {
	
	for i := 0; i < b.N; i++ {

		_,err := parse("PxrCamera", []byte(argsExample1))
		if err != nil {
			b.Error(err)
		}
	}
}

func Benchmark_FilePxrCameraNoCache(b *testing.B) {

	b.StopTimer()
	Configure(Configuration{Cache: false})
	b.StartTimer()

	for i := 0; i < b.N; i++ {

		_,err := Load("PxrCamera")
		if err != nil {
			b.Error(err)
		}
	}
}

func Benchmark_FilePxrCameraWithCache(b *testing.B) {

	b.StopTimer()
	Configure(Configuration{Cache: true})
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		_,err := Load("PxrCamera")
		if err != nil {
			b.Error(err)
		}	
	}
}



/* Taken directly from PxrCamera.args, Copyright Pixar */
const argsExample1 = `
<args format="1.0">
  <shaderType>
    <tag value="projection"/>
  </shaderType>
  <help>
    A camera model that approximates a number of real world physical
    effects.  This supports all of the traditional prman perspective camera
    settings including shaped motion blur and bokeh.
  </help>
  <page name="Standard Perspective" open="True">
    <param name="fov" label="Field of View" type="float" widget="default"
           default="90.0" min="1.0" max="135.0"
           connectable="False">      
    </param>
  </page>
  <page name="Tilt-Shift" open="False">
    <param name="tilt" label="Tilt Angle" type="float" widget="default"
           default="0.0" min="-20.0" max="20.0"
           connectable="False">      
    </param>
    <param name="roll" label="Roll Angle" type="float" widget="default"
           default="0.0" min="-180.0" max="180.0"
           connectable="False">
      <help>
        Roll the lens clockwise.  If the lens tilt is non-zero this can be
        used to rotate the plane of focus around the image center.
      </help>
    </param>
    <param name="shiftX" label="Shift X" type="float" widget="default"
           default="0.0" min="-1.0" max="1.0"
           connectable="False">
      <help>
        Shift the lens horizontally.  This can be used to correct for
        perspective distortion.  Positive values shift towards the right.
      </help>
    </param>
    <param name="shiftY" label="Shift Y" type="float" widget="default"
           default="0.0" min="-1.0" max="1.0"
           connectable="False">      
    </param>
  </page>
  <page name="Lens Distortion" open="False">
    <param name="radial1" label="Radial Distortion 1" type="float" widget="default"
           default="0.0" min="-0.3" max="0.3"
           connectable="False">      
    </param>
    <param name="radial2" label="Radial Distortion 2" type="float" widget="default"
           default="0.0" min="-0.3" max="0.3"
           connectable="False">      
    </param>
    <param name="assymX" label="Assymetric Distortion X" type="float" widget="default"
           default="0.0" min="-0.3" max="0.3"
           connectable="False">      
    </param>
    <param name="assymY" label="Assymetric Distortion Y" type="float" widget="default"
           default="0.0" min="-0.3" max="0.3"
           connectable="False">     
    </param>
    <param name="squeeze" label="Anamorphic Squeeze" type="float" widget="default"
           default="1.0" min="0.5" max="2.0"
           connectable="False">     
    </param>
  </page>
  <page name="Chromatic Aberration" open="False">
    <param name="transverse" label="Transverse" type="color" widget="default"
           default="1.0 1.0 1.0"
           connectable="False">     
    </param>
    <param name="axial" label="Axial" type="color" widget="default"
           default="0.0 0.0 0.0"
           connectable="False">     
    </param>
  </page>
  <page name="Vignetting" open="False">
    <param name="natural" label="Natural" type="float" widget="default"
           default="0.0" min="0.0" max="1.0"
           connectable="False">     
    </param>
    <param name="optical" label="Optical" type="float" widget="default"
           default="0.0" min="0.0" max="1.0"
           connectable="False">      
    </param>
  </page>
  <page name="Shutter" open="False">
    <param name="sweep" label="Sweep" type="string" widget="default"
           default="global"
           connectable="False">
      <hintdict name="options">
        <string name="down" value="down"/>
        <string name="right" value="right"/>
        <string name="up" value="up"/>
        <string name="left" value="left"/>
      </hintdict>     
    </param>
    <param name="duration" label="Duration" type="float" widget="default"
           default="1.0" min="0.0" max="1.0"
           connectable="False">     
    </param>
  </page>
</args>
`
