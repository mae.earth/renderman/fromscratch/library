package plugins

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"sync"
	"path/filepath"
	"reflect"

	"gitlab.com/mae.earth/renderman/fromscratch/library/rispec"
)

type Configuration struct {
	Cache bool 
}

type File struct {
	FilePath string
	Content []byte

	Plugin *Plugin
}

var internal struct {
	sync.RWMutex	
	cache bool

	files map[string]*File
}

func init() {

	internal.cache = true
	internal.files = make(map[string]*File,0)
}

/* Configure */
func Configure(config Configuration) {
	internal.Lock()
	defer internal.Unlock()

	internal.cache = config.Cache
	internal.files = make(map[string]*File,0)
}



/* Ouput */
type Output struct {
	Name  string
	Types []string
}

/* Param */
type Param struct {
	Label       string
	Name        string
	Type        string
	Default     interface{}
	Current     interface{} /* if current  == Default then default */
	Min         interface{}
	Max         interface{}
	Widget      string
	Help        string
	Connectable bool
}

/* Page */
type Page struct {
	Name   string
	Open   bool
	Params []Param
}

/* Plugin */
type Plugin struct {
	ShaderType     string
	NodeId         string
	Name           string
	Classification string
	Help           string

	Pages []Page

	Params  []Param
	Outputs []Output
}

func (plugin *Plugin) ShortName() string {

	return strings.TrimSuffix(filepath.Base(plugin.Name),".args")	
}

func (plugin *Plugin) Parameterlist() []rispec.Rter {

	out := make([]rispec.Rter, 0)

	for _, param := range plugin.Params {
		if param.Default != param.Current {

			out = append(out, rispec.RtToken(fmt.Sprintf("%s %s", param.Type, param.Name)))

			switch param.Type {
			case "float":
				out = append(out, rispec.RtFloat(param.Current.(float64)))
				break
			case "int":
				out = append(out, rispec.RtInt(param.Current.(int)))
				break
			case "color":
				out = append(out, rispec.RtColor(param.Current.([3]float64)))
				break
			case "normal":
				out = append(out, rispec.RtNormal(param.Current.([3]float64)))
				break
			case "vector":
				out = append(out, rispec.RtVector(param.Current.([3]float64)))
				break
			case "point":
				out = append(out, rispec.RtPoint(param.Current.([3]float64)))
				break
			case "string":
				out = append(out, rispec.RtString(param.Current.(string)))
				break
			}
		}
	}

	for _,page := range plugin.Pages {
		for _, param := range page.Params {
			if param.Default != param.Current {

				out = append(out, rispec.RtToken(fmt.Sprintf("%s %s", param.Type, param.Name)))

				switch param.Type {
				case "float":
					out = append(out, rispec.RtFloat(param.Current.(float64)))
					break
				case "int":
					out = append(out, rispec.RtInt(param.Current.(int)))
					break
				case "color":
					out = append(out, rispec.RtColor(param.Current.([3]float64)))
					break
				case "normal":
					out = append(out, rispec.RtNormal(param.Current.([3]float64)))
					break
				case "vector":
					out = append(out, rispec.RtVector(param.Current.([3]float64)))
					break
				case "point":
					out = append(out, rispec.RtPoint(param.Current.([3]float64)))
					break
				case "string":
					out = append(out, rispec.RtString(param.Current.(string)))
					break
				}
			}
		}
	}


	return out
}

/* Lookup */
func (plugin *Plugin) Lookup(name string) (Param, bool) {

	for _, param := range plugin.Params {
		if param.Name == name {
			return Param{Label: param.Label, Name: param.Name, Type: param.Type,
				Default: param.Default, Current: param.Current, Min: param.Min,
				Max: param.Max, Widget: param.Widget, Help: param.Help, Connectable: param.Connectable}, true
		}
	}

	for _, page := range plugin.Pages {
		for _, param := range page.Params {
			if param.Name == name {
				return Param{Label: param.Label, Name: param.Name, Type: param.Type,
					Default: param.Default, Current: param.Current, Min: param.Min,
					Max: param.Max, Widget: param.Widget, Help: param.Help,
					Connectable: param.Connectable}, true
			}
		}
	}
	return Param{}, false
}

/* Set */
func (plugin *Plugin) Set(toset ...interface{}) *Plugin {

	adjusts := make([]Param, 0)
	label := "--"
	for i, part := range toset {
		switch reflect.TypeOf(part).String() {
		case "string":
			if i%2 == 0 {
				label = part.(string)
			} else {
				str := part.(string)
				adjusts = append(adjusts, Param{Name: label, Type: "string", Default: str})
			}
			break
		case "bool": /* FIXME: is bool in the args spec ? */
			if i%2 != 0 {
				b := part.(bool)
				adjusts = append(adjusts, Param{Name: label, Type: "bool", Default: b})
			}
			break
		case "int":
			if i%2 != 0 {
				b := part.(int)
				adjusts = append(adjusts, Param{Name: label, Type: "int", Default: b})
			}
			break
		case "float64":
			if i%2 != 0 {
				b := part.(float64)
				adjusts = append(adjusts, Param{Name: label, Type: "float", Default: b})
			}
			break
		case "[3]float64":
			if i%2 != 0 {
				b := part.([3]float64)
				adjusts = append(adjusts, Param{Name: label, Type: "triple", Default: b})
			}
			break
		/* TODO add [6] and [16] */
		}
	}

	/* deep copy */

	n := new(Plugin)
	n.ShaderType = plugin.ShaderType
	n.NodeId = plugin.NodeId
	n.Name = plugin.Name
	n.Classification = plugin.Classification
	n.Help = plugin.Help

	n.Pages = make([]Page, 0)
	for _, page := range plugin.Pages {

		params := make([]Param, 0)
		for _, param := range page.Params {

			/* check if in the adjusts list */
			var adj *Param
			for _, adjust := range adjusts {
				if adjust.Name == param.Name {
					adj = &adjust
					break
				}
			}

			var current = param.Current

			if adj != nil && (adj.Type == param.Type || adj.Type == "triple") {
				current = adj.Default

				/* TODO: check min/max */

			}

			cparam := Param{Label: param.Label, Name: param.Name, Type: param.Type, Current: current,
				Default: param.Default, Min: param.Min, Max: param.Max,
				Widget: param.Widget, Help: param.Help, Connectable: param.Connectable}

			params = append(params, cparam)
		}

		p := Page{Name: page.Name, Open: page.Open, Params: params}
		n.Pages = append(n.Pages, p)
	}

	n.Params = make([]Param, 0)
	for _, param := range plugin.Params {
		/* check if in the adjusts list */
		var adj *Param
		for _, adjust := range adjusts {
			if adjust.Name == param.Name {
				adj = &adjust
				break
			}
		}

		var current = param.Current

		if adj != nil && (adj.Type == param.Type || adj.Type == "triple") {
			current = adj.Default

			/* TODO: check min/max */
		}

		cparam := Param{Label: param.Label, Name: param.Name, Type: param.Type, Current: current,
			Default: param.Default, Min: param.Min, Max: param.Max,
			Widget: param.Widget, Help: param.Help, Connectable: param.Connectable}

		n.Params = append(n.Params, cparam)
	}

	n.Outputs = make([]Output, 0)
	for _, output := range plugin.Outputs {
		coutput := Output{Name: output.Name, Types: output.Types}
		n.Outputs = append(n.Outputs, coutput)
	}

	return n
}

/* Load */
func Load(target string) (*Plugin, error) {

	filepath := target

	if !path.IsAbs(target) {

		rmantree := os.Getenv("RMANTREE")
		if len(rmantree) == 0 {
			return nil, ErrBadTree
		}

		filepath = path.Clean(path.Join(rmantree, "/lib/plugins/Args/", target))
	}

	if path.Ext(filepath) != ".args" {
		filepath += ".args"
	}

	internal.RLock()
	if internal.cache {
		if f,exists := internal.files[ filepath ]; exists {
			if f.Plugin != nil {
				plugin := f.Plugin.Set()
				internal.RUnlock()
				return plugin,nil
			}
		}
	}
	internal.RUnlock()

	file, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}

	plugin, err := parse(target, file)
	if err != nil {
		return nil, err
	}

	internal.Lock()
	defer internal.Unlock()
	
	if internal.cache {

		f := &File{FilePath: filepath, Plugin: plugin.Set()}
		f.Content = make([]byte,len(file))
		if len(file) > 0 {
			copy(f.Content,file)
		}

		internal.files[ filepath ] = f
	}

	return plugin, nil
}

/* information */
type Information struct {
	Types map[string]int
	NamesByType map[string][]string
}

func (info *Information) PrettyPrint() string {
	
	pural := func(n int,v string) string {
		if n == 1 {
			return v
		}
		if v[len(v) - 1] == 's' {
			return v
		}
		return v + "s"
	}


	out := "ShaderTypes:\n"
	for name,count := range info.Types {
		out += fmt.Sprintf("%03d %s\n",count,pural(count,name))
	}

	out += "\n"
	for name,names := range info.NamesByType {
		out += fmt.Sprintf("   %s: %s\n\n",name,strings.Join(names,","))
	}

	return out
}

/* Scan */
func Scan(dir string) (*Information,error) {

	target := dir

	if len(target) == 0 {
		rmantree := os.Getenv("RMANTREE")
		if len(rmantree) == 0 {
			return nil,ErrBadTree
		}

		target = path.Clean(path.Join(rmantree, "/lib/plugins/Args/"))
	}

	/* check that filepath is a directory */

	plugins := make([]*Plugin,0)
	mux := new(sync.RWMutex)

	/* walk the directory for .args files */
	walk := func(path string, info os.FileInfo,err error) error {
		if !info.IsDir() {
			plugin,err := Load(path)
			if err != nil {
				return fmt.Errorf("plugin %q parser error -- %v",path,err)
			}
			mux.Lock()
			defer mux.Unlock()

			plugins = append(plugins,plugin)
		}
		return nil
	}

	/* for each .arg file, parse and gather information about said plugin */
	if err := filepath.Walk(target,walk); err != nil {
		return nil,err
	}

	fmt.Printf("Scanned %q -- loaded %d plugins\n",target,len(plugins))

	info := &Information{}
	info.Types = make(map[string]int,0)
	info.NamesByType = make(map[string][]string,0)

	for _,plugin := range plugins {

		if _,exists := info.Types[ plugin.ShaderType ]; !exists {
			info.Types[ plugin.ShaderType ] = 0
		
			info.NamesByType[ plugin.ShaderType ] = make([]string,0)

		}

		info.Types[ plugin.ShaderType ]++
		info.NamesByType[ plugin.ShaderType ] = append(info.NamesByType[ plugin.ShaderType ],plugin.ShortName())

	}


	return info,nil
}



