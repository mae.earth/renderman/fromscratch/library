package plugins

import (
	"encoding/xml"
	"errors"
	"fmt"
	"strconv"
	"strings"
)

var (
	ErrSkipParam = errors.New("Skip Param")
	ErrBadParam  = errors.New("Bad Param")
	ErrBadTree   = errors.New("Bad Tree")
)

type argsTag struct {
	XMLName xml.Name `xml:"tag"`
	Value   string   `xml:"value,attr"`
}

type argsShaderType struct {
	XMLName xml.Name `xml:"shaderType"`
	Tag     argsTag  `xml:"tag"`
}

type argsTags struct {
	XMLName xml.Name  `xml:"tags"`
	Tags    []argsTag `xml:"tag"`
}

type argsHelp struct {
	XMLName xml.Name `xml:"help"`
	Value   string   `xml:",innerxml"`
}

type argsString struct {
	XMLName xml.Name `xml:"string"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:"value,attr"`
}

type argsHintDict struct {
	XMLName    xml.Name     `xml:"hintdict"`
	Attributes []argsString `xml:"string"`
	Name       string       `xml:"name,attr"`
}

type argsParam struct {
	XMLName xml.Name `xml:"param"`
	Tags    argsTags `xml:"tags"`
	Help    argsHelp `xml:"help"`

	Label       string `xml:"label,attr"`
	Name        string `xml:"name,attr"`
	Type        string `xml:"type,attr"`
	Default     string `xml:"default,attr"`
	Min         string `xml:"min,attr"`
	Max         string `xml:"max,attr"`
	Widget      string `xml:"widget,attr"`
	Connectable string `xml:"connectable,attr"`
	ArraySize   string `xml:"arraySize,attr"`
	IsDynamicArray string `xml:"isDynamicArray,attr"`
}

type argsOutput struct {
	XMLName xml.Name `xml:"output"`
	Name    string   `xml:"name,attr"`
	Tags    argsTags `xml:"tags"`
}

type argsRfmdata struct {
	XMLName xml.Name `xml:"rfmdata"`

	NodeId         string `xml:"nodeid,attr"`
	Classification string `xml:"classification,attr"`
}

type argsPage struct {
	XMLName xml.Name    `xml:"page"`
	Name    string      `xml:"name,attr"`
	Open    string      `xml:"open,attr"`
	Params  []argsParam `xml:"param"`
}

type args struct {
	XMLName xml.Name       `xml:"args"`
	Shader  argsShaderType `xml:"shaderType"`
	Pages   []argsPage     `xml:"page"`
	Params  []argsParam    `xml:"param"`
	Outputs []argsOutput   `xml:"output"`
	Rfmdata argsRfmdata    `xml:"rfmdata"`
	Help    argsHelp       `xml:"help"`
	Format  string         `xml:"format,attr"`
}

func parsexml(data []byte) (*args, error) {

	var a args
	if err := xml.Unmarshal(data, &a); err != nil {
		return nil, fmt.Errorf("parsexml error -- %v", err)
	}
	return &a, nil
}

func parseArgsParam(param *argsParam) (Param, error) {
	if param == nil {
		return Param{}, ErrBadParam
	}

	str2triple := func(str string) [3]float64 {

		parts := strings.Split(strings.TrimSpace(str), " ")
		if len(parts) != 3 {
			return [3]float64{0, 0, 0}
		}
		out := [3]float64{0, 0, 0}

		for i, part := range parts {
			if f, err := strconv.ParseFloat(part, 64); err != nil {
				continue
			} else {
				out[i] = f
			}
		}
		return out
	}

	var info Param

	var def interface{}
	var min interface{}
	var max interface{}

	switch param.Type {
	case "float":
		/* we need to check the arraySize first before deciding what parsing route to take */
		switch param.ArraySize {
			case "":
			def = float64(0.0)
			min = float64(0.0)
			max = float64(0.0)

			param.Default = strings.Replace(param.Default, "f", "", -1)

			if len(param.Default) > 0 {
				if f, err := strconv.ParseFloat(param.Default, 64); err != nil {
					return info, fmt.Errorf("arraySize=%q error -- %v",param.ArraySize,err)
				} else {
					def = f
				}
			}

			param.Min = strings.Replace(param.Min,"f","",-1)

			if len(param.Min) > 0 {
				if f, err := strconv.ParseFloat(param.Min, 64); err != nil {
					return info, err
				} else {
					min = f
				}
			}

			param.Max = strings.Replace(param.Max,"f","",-1)

			if len(param.Max) > 0 {
				if f, err := strconv.ParseFloat(param.Max, 64); err != nil {
					return info, err
				} else {
					max = f
				}
			}
			break
			case "-1":
				/* will be a dynamic array */
			break
			case "6":

			break
			case "16":

			break
			default:
				return info,fmt.Errorf("no parser for arraySize=%q",param.ArraySize)		
			break
		}
		break
	case "int":
		def = int(0)
		min = int(0)
		max = int(0)

		if len(param.Default) > 0 {
			if i, err := strconv.Atoi(param.Default); err != nil {
				return info, err
			} else {
				def = i
			}
		}
		if len(param.Min) > 0 {
			if i, err := strconv.Atoi(param.Min); err != nil {
				return info, err
			} else {
				min = i
			}
		}
		if len(param.Max) > 0 {
			if i, err := strconv.Atoi(param.Max); err != nil {
				return info, err
			} else {
				max = i
			}
		}
		break
	case "color", "normal", "vector", "point":
		def = [3]float64{0, 0, 0}
		min = [3]float64{0, 0, 0}
		max = [3]float64{0, 0, 0}

		if len(param.Default) > 0 {
			def = str2triple(param.Default)
		}
		if len(param.Min) > 0 {
			min = str2triple(param.Min)
		}
		if len(param.Max) > 0 {
			max = str2triple(param.Max)
		}
		break
	case "string":
		def = param.Default
		min = param.Min
		max = param.Max
		break
	case "struct", "lightfilter","displayfilter","samplefilter":
		/* TODO: all these need completing */
		return info, ErrSkipParam
		break
	default:
		return info, fmt.Errorf("unknown %s=[%s]", param.Name, param.Type)
		break
	}

	info.Type = param.Type
	info.Label = param.Label
	info.Name = param.Name
	info.Widget = param.Widget
	info.Help = strings.TrimSpace(param.Help.Value)
	info.Default = def
	info.Current = def /* means the default will be used */
	info.Min = min
	info.Max = max
	info.Connectable = false
	if param.Connectable == "True" {
		info.Connectable = true
	}

	return info, nil
}

func parse(name string, data []byte) (*Plugin, error) {

	args, err := parsexml(data)
	if err != nil {
		return nil, err
	}

	plugin := new(Plugin)
	plugin.ShaderType = args.Shader.Tag.Value
	plugin.NodeId = args.Rfmdata.NodeId
	plugin.Name = name
	plugin.Classification = args.Rfmdata.Classification
	plugin.Help = strings.TrimSpace(args.Help.Value)

	plugin.Pages = make([]Page, 0)

	for _, page := range args.Pages {
		p := Page{}
		p.Name = page.Name
		p.Open = true
		if page.Open == "False" {
			p.Open = false
		}
		p.Params = make([]Param, 0)

		for _, param := range page.Params {

			info, err := parseArgsParam(&param)
			if err != nil {
				if err == ErrSkipParam {
					continue
				}
				return nil, err
			}

			p.Params = append(p.Params, info)
		}

		plugin.Pages = append(plugin.Pages, p)
	}

	plugin.Params = make([]Param, 0)

	for _, param := range args.Params {
		info, err := parseArgsParam(&param)
		if err != nil {
			if err == ErrSkipParam {
				continue
			}
			return nil, err
		}

		plugin.Params = append(plugin.Params, info)
	}

	plugin.Outputs = make([]Output, 0)

	for _, output := range args.Outputs {
		op := Output{}
		op.Name = output.Name
		op.Types = make([]string, 0)
		for _, t := range output.Tags.Tags {
			op.Types = append(op.Types, t.Value)
		}

		plugin.Outputs = append(plugin.Outputs, op)
	}

	return plugin, nil
}
