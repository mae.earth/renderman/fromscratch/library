package rib

import (
	"fmt"
	"bytes"
	"reflect"

	"gitlab.com/mae.earth/renderman/fromscratch/library/rispec"
	"gitlab.com/mae.earth/renderman/fromscratch/library/format"
)

/* convert natural golang to rispec based on command */
func Values(command string,values ...Param) ([]rispec.Rter,error) {

	out := make([]rispec.Rter,0)	

	for _,param := range values {

		/* TODO: read the spec struct for information about what the function should expect */

		switch reflect.TypeOf(param.Value).String() {
			case "int":
				out = append(out,rispec.RtInt(param.Value.(int)))
			break
			case "float64":
				out = append(out,rispec.RtFloat(param.Value.(float64)))
			break
			case "[2]float64":
				out = append(out,rispec.RtInterval(param.Value.([2]float64)))
			break
			case "[3]float64": /* amb. could be color, vector, point... */
				out = append(out,rispec.RtVector(param.Value.([3]float64)))
			break
			case "[6]float64":
				out = append(out,rispec.RtBound(param.Value.([6]float64)))
			break
			case "[16]float64": /* amb. could be matrix or basis */
				out = append(out,rispec.RtMatrix(param.Value.([16]float64)))
			break
			/* TODO: and the rest */
			case "string": /* amb. could be token */
				out = append(out,rispec.RtString(param.Value.(string)))
			break
			default:
				return nil,fmt.Errorf("value %q unknown",reflect.TypeOf(param.Value))
			break
		}
	}

	return out,nil
}


/* convert the natural golang to rispec */
func Params(params ...Param) ([]rispec.Rter,error) {

	out := make([]rispec.Rter,0)

	for _,param := range params {

		/* TODO: parse the specification token for the type etc */

		out = append(out,rispec.RtToken(param.Name))

		switch reflect.TypeOf(param.Value).String() {
			case "int":
				out = append(out,rispec.RtInt(param.Value.(int)))
			break
			case "float64":				
				out = append(out,rispec.RtFloat(param.Value.(float64)))
			break
			case "[2]float64":
				out = append(out,rispec.RtInterval(param.Value.([2]float64)))
			break
			case "[3]float64":
				out = append(out,rispec.RtVector(param.Value.([3]float64)))
			break
			case "[6]float64":
				out = append(out,rispec.RtBound(param.Value.([6]float64)))
			break
			case "[16]float64": /* amb. could be matrix or basis */
				out = append(out,rispec.RtMatrix(param.Value.([16]float64)))
			break
			/* TODO: and the rest */
			case "string":
				out = append(out,rispec.RtString(param.Value.(string)))
			break
			default:
				return nil,fmt.Errorf("param %q unknown",reflect.TypeOf(param.Value))
			break
		}
	}

	return out,nil
}

/* Options */
type Options struct {
	PrettyPrint bool

	depth int
}

const tabs string = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"

/* RIB */
func RIB(options Options,blocks ...interface{}) ([]byte,error) {
	
	buf := bytes.NewBuffer(nil)
	
	for _,block := range blocks {

		switch reflect.TypeOf(block).String() {
			case "*rib.ArchiveBlock":
		
				archive := block.(*ArchiveBlock)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				
				buf.Write([]byte(fmt.Sprintf("ArchiveBlock %q\n",archive.Name)))
				
				b,err := RIB(Options{PrettyPrint: options.PrettyPrint, depth:options.depth + 1}, archive.Children...)
				if err != nil {
					return nil,err
				}	

				buf.Write(b)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("ArchiveEnd\n"))

			break
			case "*rib.FrameBlock":

				frame := block.(*FrameBlock)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				buf.Write([]byte(fmt.Sprintf("FrameBegin %d\n",frame.Frame)))

				b,err := RIB(Options{PrettyPrint: options.PrettyPrint, depth:options.depth + 1}, frame.Children...)
				if err != nil {
					return nil,err
				}

				buf.Write(b)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				buf.Write([]byte("FrameEnd\n"))
			break
			case "*rib.WorldBlock":

				world := block.(*WorldBlock)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				buf.Write([]byte("WorldBegin\n"))

				b,err := RIB(Options{PrettyPrint: options.PrettyPrint, depth:options.depth + 1}, world.Children...)
				if err != nil {
					return nil,err
				}

				buf.Write(b)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				buf.Write([]byte("WorldEnd\n"))
			break
			case "*rib.AttributeBlock":

				attribute := block.(*AttributeBlock)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("AttributeBegin\n"))
			
				b,err := RIB(Options{PrettyPrint: options.PrettyPrint, depth:options.depth + 1}, attribute.Children...)
				if err != nil {
					return nil,err
				}

				buf.Write(b)

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("AttributeEnd\n"))
			break
			case "*rib.Record":
				/* RiArchiveRecord ( RtToken type, char *format [, arg ...] ) 
					This call writes a user data record (data which is outside the scope of the requests
described in the rest of Part I of this document) into a RIB archive file or stream.
type is one of ”comment”, or ”structure”, or ”verbatim”. ”comment” begins the user
data record with a RIB comment marker and terminates it with a newline. ”structure”
begins the user data record with a RIB structuring convention preface and terminates
it with a newline. ”verbatim” just outputs the data as-is, with no leading comment
character and no trailing newline. The user data record itself is supplied as a printf()
format string with optional arguments. It is an error to embed newline characters in
the format or any of its string arguments.

					*/

				record := block.(*Record)

				switch record.Type {
					case Structure:
						if options.PrettyPrint && options.depth > 0 {
							buf.Write([]byte(tabs[:options.depth]))
						}
						buf.Write([]byte(fmt.Sprintf("##" + record.Format + "\n", record.Args...)))
					break
					case Comment:
						if options.PrettyPrint && options.depth > 0 {
							buf.Write([]byte(tabs[:options.depth]))
						}
						buf.Write([]byte(fmt.Sprintf("# " + record.Format + "\n", record.Args...)))
					break
					case Verbatim:
						buf.Write([]byte(fmt.Sprintf(record.Format,record.Args...)))
					break
					default: 

						return nil,fmt.Errorf("record %q unknown",record.Type)
					break
				}				

			break
			case "*rib.General":

				general := block.(*General)

				vs,err := Values(general.RIB,general.Values...)
				if err != nil {
					return nil,err
				}

				ps,err := Params(general.Params...) 
			  if err != nil {
					return nil,err
				}

				if options.PrettyPrint && options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}


				buf.Write([]byte(general.RIB))

				if len(vs) > 0 {
					
					for _,param := range vs {
						buf.Write([]byte(fmt.Sprintf(" %s",param)))
					}
				}

				if len(ps) > 0 {
					for i,param := range ps {
						if i % 2 != 0 {
							buf.Write([]byte(fmt.Sprintf(" %s",rispec.Data(param))))
						} else {
							buf.Write([]byte(fmt.Sprintf(" %s",param)))
						}
					}
				}

				buf.Write([]byte("\n"))
			break
			default:
				buf.Write([]byte(fmt.Sprintf("%s\n",reflect.TypeOf(block))))
			break
		}

	}

	return buf.Bytes(),nil
}

/* scanner goes through the RIB blocks and constructs some information
 * as an end result string; however it stops at the first sign of another
 * block. 
 */
func scanner(options Options,root interface{}) string {

	if root == nil {
		return ""
	}

	blockf := func(b interface{}) (string,bool) {
		if reflect.TypeOf(b).String() != "*rib.General" {
			return "",false
		}
		block := b.(*General)

		out := ""
		
		switch block.RIB {
			case "Attribute": 
				
				/* TODO: assumption made here */
				if block.Values[0].Value.(string) == "identifier" {
					out = fmt.Sprintf("%q",block.Params[0].Value.(string))
				}			

			break
		}		

		if len(out) == 0 {
			return "",false
		}

		return out,true
	}

	framef := func(b interface{}) (string,bool) {
		if reflect.TypeOf(b).String() != "*rib.General" {
			return "",false
		}
		block := b.(*General)

		out := ""

		switch block.RIB {
			case "Display":
				
				/* TODO : instead of guess the values order, use a lookup function */
				filename := block.Values[0].Value.(string)
				channels := block.Values[1].Value.(string)

				out = fmt.Sprintf("%q@%q",filename,channels)

			break
			case "Format":

				/* TODO : as above */
				width := block.Values[0].Value.(int)
				height := block.Values[1].Value.(int)
				pixelaspect := block.Values[2].Value.(float64)

				out = fmt.Sprintf("%d x %d : %s",width,height,format.Float64(pixelaspect))
			break
		}

		if len(out) == 0 {
			return "",false
		}

		return out,true
	}
	
		
	out := ""

	switch reflect.TypeOf(root).String() {
		case "*rib.FrameBlock":

			frame := root.(*FrameBlock)

			for _,block := range frame.Children {
				if str,ok := framef(block); ok {
					out += "<span class=\"param\">" + str + "</span>"					
				}				
			}
		break
		case "*rib.WorldBlock":
			world := root.(*WorldBlock)
			for _,block := range world.Children {
				if str,ok := blockf(block); ok {
					out += "<span class=\"param\">" + str + "</span>"
				}			
			}
		break
		case "*rib.AttributeBlock":
			attr := root.(*AttributeBlock)
			for _,block := range attr.Children {
				if str,ok := blockf(block); ok {
					out += "<span class=\"param\">" + str + "</span>"
				}			
			}
		break
		case "*rib.ArchiveBlock":
			arc := root.(*ArchiveBlock)
			for _,block := range arc.Children {
				if str,ok := blockf(block); ok {
					out += "<span class=\"param\">" + str + "</span>"
				}
			}
		break
	}
	
	return out
}


/* HTML */
func HTML(options Options,blocks ...interface{}) ([]byte,error) {
	
	buf := bytes.NewBuffer(nil)
	if options.depth == 0 {
		buf.Write([]byte("<div class=\"rib\">\n<ul class=\"statements\">\n"))	
		options = Options{depth:1}
	}

	justvalues := func(list []rispec.Rter) string {
		if len(list) == 0 {
			return ""
		}
		out := ""
		for _,param := range list {
			out += " " + param.String()
		}
		return out
	}

	paramstostring := func(list []rispec.Rter) string {
		if len(list) == 0 {
			return ""
		}
		out := ""
		for i,param := range list {
			if i % 2 == 0 {	
				out += fmt.Sprintf("<span class=\"token\">%s</span>",param)
			} else {
				out += fmt.Sprintf("<span class=\"data\">%s</span>",rispec.Data(param))
			}
		}
		return out
	}
			
	
	for _,block := range blocks {

		switch reflect.TypeOf(block).String() {
			case "*rib.ArchiveBlock":
		
				archive := block.(*ArchiveBlock)

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				results := scanner(options,archive)
				
				buf.Write([]byte(fmt.Sprintf(`<li class="archive"><span class="title">Archive %q</span><span>%s</span>`,archive.Name,results)))
				
				if len(archive.Children) > 0 {
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte(fmt.Sprintf("<ul data-depth=\"%d\">\n",options.depth)))
					b,err := HTML(Options{depth:options.depth + 1}, archive.Children...)
					if err != nil {
						return nil,err
					}	

					buf.Write(b)
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte("</ul>\n"))
				}

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("</li>\n"))
	
			break
			case "*rib.FrameBlock":

				frame := block.(*FrameBlock)

				if  options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
	
				results := scanner(options,frame)
		
				buf.Write([]byte(fmt.Sprintf(`<li class="frame"><span class="title">Frame %d</span><span>%s</span>`,frame.Frame,results)))

				if len(frame.Children) > 0 {
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte(fmt.Sprintf("<ul data-depth=\"%d\">\n",options.depth)))
					b,err := HTML(Options{depth:options.depth + 1}, frame.Children...)
					if err != nil {
						return nil,err
					}

					buf.Write(b)
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte("</ul>\n"))
				}

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("</li>\n"))
			break
			case "*rib.WorldBlock":

				world := block.(*WorldBlock)

  			if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				results := scanner(options,world)

				buf.Write([]byte(fmt.Sprintf(`<li class="world"><span class="title">World</span><span>%s</span>`,results)))

				if len(world.Children) > 0 {
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte(fmt.Sprintf("<ul data-depth=\"%d\">\n",options.depth)))
					b,err := HTML(Options{depth:options.depth + 1}, world.Children...)
					if err != nil {
						return nil,err
					}

					buf.Write(b)
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}		
					buf.Write([]byte("</ul>\n"))
				}

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				buf.Write([]byte("</li>\n"))
			break
			case "*rib.AttributeBlock":

				attribute := block.(*AttributeBlock)

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				results := scanner(options,attribute)

				buf.Write([]byte(fmt.Sprintf(`<li class="attribute"><span class="title">Attribute</span><span>%s</span>`,results)))
			
				if len(attribute.Children) > 0 {
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}				

					buf.Write([]byte(fmt.Sprintf("<ul data-depth=\"%d\">\n",options.depth)))
					b,err := HTML(Options{depth:options.depth + 1}, attribute.Children...)
					if err != nil {
						return nil,err
					}

					buf.Write(b)
					if options.depth > 0 {
						buf.Write([]byte(tabs[:options.depth]))
					}
					buf.Write([]byte("</ul>\n"))
				}

				if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}
				
				buf.Write([]byte("</li>\n"))
			break
			case "*rib.Record":
				/* RiArchiveRecord ( RtToken type, char *format [, arg ...] ) 
					This call writes a user data record (data which is outside the scope of the requests
described in the rest of Part I of this document) into a RIB archive file or stream.
type is one of ”comment”, or ”structure”, or ”verbatim”. ”comment” begins the user
data record with a RIB comment marker and terminates it with a newline. ”structure”
begins the user data record with a RIB structuring convention preface and terminates
it with a newline. ”verbatim” just outputs the data as-is, with no leading comment
character and no trailing newline. The user data record itself is supplied as a printf()
format string with optional arguments. It is an error to embed newline characters in
the format or any of its string arguments.

					*/

				record := block.(*Record)

					if options.depth > 0 {
					buf.Write([]byte(tabs[:options.depth]))
				}

				switch record.Type {
					case Structure:
						buf.Write([]byte(fmt.Sprintf(`<li class="structure">` + record.Format + "</li>\n", record.Args...)))
					break
					case Comment:
						buf.Write([]byte(fmt.Sprintf(`<li class="comment">` + record.Format + "</li>\n", record.Args...)))
					break
					case Verbatim:
						buf.Write([]byte("<li class=\"verbatim\">\n"))
						buf.Write([]byte(fmt.Sprintf(record.Format,record.Args...)))
						if options.depth > 0 {
							buf.Write([]byte(tabs[:options.depth]))
						}
						buf.Write([]byte("</li>\n"))
					break
					default: 

						return nil,fmt.Errorf("record %q unknown",record.Type)
					break
				}				

			break
			case "*rib.General":

				/* we scan the general structure but do not actually render */

			
				general := block.(*General)

				vs,err := Values(general.RIB,general.Values...)
				if err != nil {
					return nil,err
				}

				ps,err := Params(general.Params...)
				if err != nil {
					return nil,err
				}

				/* Look for actual assets */
				switch general.RIB {
					case "Sphere":
					
						if options.depth > 0 {
							buf.Write([]byte(tabs[:options.depth]))
						}

						buf.Write([]byte(fmt.Sprintf("<li class=\"with-image asset\"><span class=\"title\">Sphere</span><span class=\"values\">%s</span>\n",justvalues(vs))))
						buf.Write([]byte(fmt.Sprintf("<span class=\"parameterlist\">%s</span>\n",paramstostring(ps))))
						buf.Write([]byte(fmt.Sprintf("<img src=\"r/mesh.svg\"></li>\n")))

					break
					case "Light":

						if options.depth > 0 {
							buf.Write([]byte(tabs[:options.depth]))
						}

						buf.Write([]byte(fmt.Sprintf("<li class=\"with-image asset\"><span class=\"title\">Light</span><span class=\"values\">%s</span>\n",justvalues(vs))))
						buf.Write([]byte(fmt.Sprintf("<span class=\"parameterlist\">%s</span>\n",paramstostring(ps))))
						buf.Write([]byte(fmt.Sprintf("<img src=\"r/light.svg\"></li>\n")))
					break
				}

			break
			default:
				buf.Write([]byte(fmt.Sprintf("%s\n",reflect.TypeOf(block))))
			break
		}

	}

	if (options.depth - 1) == 0 {
		buf.Write([]byte("</ul>\n</div>\n"))
	}

	return buf.Bytes(),nil
}






/* RecordStructure */
func RecordStructure(format string,args ...interface{}) *Record {

	r := new(Record)
	r.Type = Structure
	r.Format = format
	r.Args = make([]interface{},len(args))
	if len(args) > 0 {
		copy(r.Args,args)
	}

	return r
}

/* RecordComment */
func RecordComment(format string,args ...interface{}) *Record {

	r := new(Record)
	r.Type = Comment
	r.Format = format
	r.Args = make([]interface{},len(args))
	if len(args) > 0 {
		copy(r.Args,args)
	}
	return r
}

/* RecordVerbatim */
func RecordVerbatim(format string,args ...interface{}) *Record {

	r := new(Record)
	r.Type = Verbatim
	r.Format = format
	r.Args = make([]interface{},len(args))
	if len(args) > 0 {
		copy(r.Args,args)
	}
	return r
}







