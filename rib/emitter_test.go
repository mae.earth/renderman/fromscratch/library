package rib

import (
	"io/ioutil"

	"bytes"
	"testing"
	. "github.com/smartystreets/goconvey/convey"
)

func Test_Emitter(t *testing.T) {
	Convey("Emitter",t,func() {

		block := new(FrameBlock)
		block.Frame = 1

		display := new(General)
		display.RIB = "Display"
		display.Values = []Param{Param{"filename","test.exr"},Param{"type","openexr"},Param{"mode","rgba"}}

		format := new(General)
		format.RIB = "Format"
		format.Values = []Param{Param{"width",640},Param{"height",480},Param{"pixelratio",1.0}}
	
		archive := new(Record)
		archive.Type = Structure
		archive.Format = "%s"
		archive.Args = []interface{}{"RenderMan RIB"}

		comment := new(Record)
		comment.Type = Comment
		comment.Format = "%s"
		comment.Args = []interface{}{"this is a sphere"}

		lightatt := new(General)
		lightatt.RIB = "Attribute"
		lightatt.Values = []Param{Param{"name","identifier"}}
		lightatt.Params = []Param{Param{"string name","key_light"}}

		light := new(General)
		light.RIB = "Light"
		light.Values = []Param{Param{"shader","PxrSphereLight"},Param{"name","light"}}
		light.Params = []Param{Param{"float intensity",8.0}}

		sphereatt := new(General)
		sphereatt.RIB = "Attribute"
		sphereatt.Values = []Param{Param{"name","identifier"}}
		sphereatt.Params = []Param{Param{"string name","hero"}}

		sphere := new(General)
		sphere.RIB = "Sphere"
		sphere.Values = []Param{Param{"radius",1.0},Param{"zmin",1.0},
													  Param{"zmax",-1.0},Param{"tmax",360.0}}
		sphere.Params = []Param{Param{"float foo",1.23},Param{"string bar","foo"}}
	
		attribute := new(AttributeBlock)
		attribute.Children = []interface{}{sphereatt,comment,sphere}

		lattribute := new(AttributeBlock)
		lattribute.Children = []interface{}{lightatt,light}

		world := new(WorldBlock)
		world.Children = []interface{}{lattribute,attribute}

		version := new(General)
		version.RIB = "version"
		version.Values = []Param{Param{"ver",3.04}}

		block.Children = []interface{}{format,display,world}

		/*
		verbatim := new(Record)
		verbatim.Type = Verbatim
		verbatim.Format = "%s"
		verbatim.Args = []interface{}{"verbatim test\n"}
		*/

		Convey("RIB",func() {

			rib,err := RIB(Options{PrettyPrint: true}, archive,version,/*verbatim,*/block)
			So(err,ShouldBeNil)
			//fmt.Printf("\n\n%s\n\n",string(rib))

			if err := ioutil.WriteFile("emitter.rib",rib,0664); err != nil {
				panic(err)
			}
		})

		Convey("HTML",func() {

			html,err := HTML(Options{},archive,version,/*verbatim,*/block)
			So(err,ShouldBeNil)
			//fmt.Printf("\n\n%s\n\n",string(html))

			content := bytes.NewBuffer([]byte(docheader))
			content.Write(html)
			content.Write([]byte(docfooter))

			if err := ioutil.WriteFile("emitter.html",content.Bytes(),0664); err != nil {
				panic(err)
			}
		})
	})
}

const docheader string = `<!DOCTYPE="html"><html><head>
<style>
/* rib stuff */
div.rib { font-size: 10px; padding: 5px; }
div.rib ul { list-style: none; padding: 5px 0px 0px; margin: 0; box-sizing: border-box; }
div.rib li { box-sizing: border-box; position: relative; }
div.rib li[class] { background-color: black; color: white; padding: 5px 0 5px 5px; width: 100%; margin: 5px 0; }
div.rib * li * li[class] { background-color: cadetblue; }
div.rib * li * li * li[class] { background-color: tan; }
div.rib * li * li * li * li[class] { background-color: tomato; }
div.rib * li.with-image { height: 100px; }
div.rib * li.with-image img { width: 100px; position: absolute; top: 10px; right: 10px;  }
div.rib li .title { margin-right: 10px; }

div.rib span.parameterlist { display: block; margin-top: 10px; }
div.rib span.parameterlist span.token { margin-right: 5px; }
div.rib span.parameterlist span.data { margin-right: 10px; }
div.rib li.structure { background-color: unset !important; color: black; border: 1px solid black; }
div.rib li.comment { font-family: monospace; font-size: 8px; width: fit-content; padding: 5px; background-color: ivory !important; color: black; }
div.rib li.asset .title { font-size: 3em; vertical-align: middle; }
div.rib li.asset .values { font-size: 1.5em; vertical-align: bottom;}
div.rib span.param { margin: 0 5px; }
</style>
</head><body>`

const docfooter string = `</body></html>`






