module mae.earth/renderman/fromscratch/library

go 1.14

require (
	github.com/smartystreets/goconvey v1.6.4
	gitlab.com/mae.earth/renderman/fromscratch/library v0.0.0-20180207204645-350cc9459b48
)
