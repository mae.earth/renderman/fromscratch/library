package format

import (
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func Test_Numbers(t *testing.T) {
	Convey("numbers.go", t, func() {
		Convey("Float64", func() {
			Convey("positive", func() {
				So(Float64(1.0), ShouldEqual, "1")
				So(Float64(0.5), ShouldEqual, ".5")
				So(Float64(1.555), ShouldEqual, "1.555")
			})
			Convey("negative", func() {
				So(Float64(-1.0), ShouldEqual, "-1")
				So(Float64(-0.5), ShouldEqual, "-.5")
				So(Float64(-1.555), ShouldEqual, "-1.555")
			})
			Convey("example output", func() {
				fmt.Printf("\ngo=%f, RIB=%s\n", 1.23, Float64(1.23))
			})
		})
		Convey("[2]Float64", func() {
			Convey("positive", func() {
				So(Float64([2]float64{1.0, 1.0}), ShouldEqual, "1 1")
				So(Float64([2]float64{0.5, 0.5}), ShouldEqual, ".5 .5")
				So(Float64([2]float64{1.555, 1.555}), ShouldEqual, "1.555 1.555")
			})
			Convey("negative", func() {
				So(Float64([2]float64{-1.0, -1.0}), ShouldEqual, "-1 -1")
				So(Float64([2]float64{-0.5, -0.5}), ShouldEqual, "-.5 -.5")
				So(Float64([2]float64{-1.555, -1.555}), ShouldEqual, "-1.555 -1.555")
			})
			Convey("missing", func() {
				So(Float64([2]float64{-.567}), ShouldEqual, "-.567 0")
			})
		})
		Convey("[3]Float64", func() {
			Convey("positive", func() {
				So(Float64([3]float64{1.0, 1.0, 1.0}), ShouldEqual, "1 1 1")
				So(Float64([3]float64{0.5, 0.5, 0.5}), ShouldEqual, ".5 .5 .5")
				So(Float64([3]float64{1.555, 1.555, 1.555}), ShouldEqual, "1.555 1.555 1.555")
			})
			Convey("negative", func() {
				So(Float64([3]float64{-1.0, -1.0, -1.0}), ShouldEqual, "-1 -1 -1")
				So(Float64([3]float64{-0.5, -0.5, -0.5}), ShouldEqual, "-.5 -.5 -.5")
				So(Float64([3]float64{-1.555, -1.555, -1.555}), ShouldEqual, "-1.555 -1.555 -1.555")
			})
			Convey("missing", func() {
				So(Float64([3]float64{-1.0, -1.0}), ShouldEqual, "-1 -1 0")
			})
		})
		Convey("[4]Float64", func() {
			Convey("positive", func() {
				So(Float64([4]float64{1.0, 1.0, 1.0, 1.0}), ShouldEqual, "1 1 1 1")
				So(Float64([4]float64{0.5, 0.5, 0.5, 0.5}), ShouldEqual, ".5 .5 .5 .5")
				So(Float64([4]float64{1.555, 1.555, 1.555, 1.555}), ShouldEqual, "1.555 1.555 1.555 1.555")
			})
		})
		Convey("[]Float64", func() {
			Convey("positive", func() {
				So(Float64([]float64{1.0, 2.0, 3.0, 4.0, 5.0}), ShouldEqual, "1 2 3 4 5")
				So(Float64v([]float64{1.0, 2.0, 3.0, 4.0, 5.0}), ShouldEqual, "1 2 3 4 5")
			})
		})
	})
}
