package format

import (
	"fmt"
	"reflect"
)

/* reduce to a compact notation in the RIB file */
func f64(f float64) string {

	str := fmt.Sprintf("%f", f)
	s := 0
	neg := false
	for i, c := range str {
		if c != '0' {
			if c == '-' {
				neg = true
				continue
			}
			s = i
			break
		}
		if c == '.' {
			break
		}
	}

	e := 0
	for i := len(str) - 1; i >= 0; i-- {
		if str[i] != '0' {
			e = i + 1
			break
		}
		if str[i] == '.' {
			break
		}
	}

	str = str[s:e]
	if str == "." {
		str = "0"
	}
	if neg {
		str = "-" + str
	}
	if str[len(str)-1] == '.' {
		str = str[:len(str)-1]
	}

	return str
}

/* Float64 */
func Float64(in interface{}) string {

	/* This function only understands the types that are required by the RISpec;
	 * float(1), color(3), point(3), vector(3), normal(3), hpoint(4),
	 * matrix(16), basis(16), bound(6) and interval(2) for anything else like a
	 * general array of floats use the Float64v() function instead.
	 */
	out := "--"

	switch reflect.TypeOf(in).String() {
	case "float64":

		f, _ := in.(float64)
		out = f64(f)
		break
	case "[2]float64":

		f, _ := in.([2]float64)
		out = fmt.Sprintf("%s %s", f64(f[0]), f64(f[1]))
		break
	case "[3]float64":

		f, _ := in.([3]float64)
		out = fmt.Sprintf("%s %s %s", f64(f[0]), f64(f[1]), f64(f[2]))
		break
	case "[4]float64":

		f, _ := in.([4]float64)
		out = fmt.Sprintf("%s %s %s %s", f64(f[0]), f64(f[1]), f64(f[2]),f64(f[3]))
		break
	case "[6]float64":

		f, _ := in.([6]float64)
		out = fmt.Sprintf("%s %s %s %s %s %s", f64(f[0]), f64(f[1]), f64(f[2]), f64(f[3]), f64(f[4]), f64(f[5]))
		break
	case "[16]float64":

		f, _ := in.([16]float64)
		out = fmt.Sprintf("%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", f64(f[0]), f64(f[1]), f64(f[2]), f64(f[3]),
			f64(f[4]), f64(f[5]), f64(f[6]), f64(f[7]),
			f64(f[8]), f64(f[9]), f64(f[10]), f64(f[11]),
			f64(f[12]), f64(f[13]), f64(f[14]), f64(f[15]))
		break
	case "[]float64":

		f, _ := in.([]float64)
		out = Float64v(f)
	break
	}

	return out
}

/* Float64v */
func Float64v(a []float64) string {

	out := ""

	for i := 0; i < len(a); i++ {
		if len(out) > 0 {
			out += " "
		}
		out += f64(a[i])
	}

	return out
}










